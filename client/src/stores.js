// @flow

import ReactDOM from "react-dom";
import * as React from "react";
import { sharedComponentData } from "react-simplified";
import axios from "axios";
axios.interceptors.response.use(response => response.data);

class News {
  id: number = 0;
  title: string = "";
  content: string = "";
  time: string = "";
  picture: string = "";
  category: string = "";
  importance: number = 0;
}

class NewsStore {
  news = [];
  latestnews = [];
  importantnews = [];
  currentNews = new News();

  getAllNews() {
    return axios.get("/news").then((news: News[]) => {
      this.news = news;
      this.importantnews = news.filter(e => e.importance == 1);
    });
  }

  getLatestNews() {
    return axios
      .get("/latestnews")
      .then((news: News[]) => (this.latestnews = news));
  }

  getTypeNews(type: string) {
    return axios.get("/news").then((news: News[]) => {
      this.news = news.filter(e => e.category == type);
    });
  }

  getNews(id: number) {
    return axios.get("/news/" + id).then(response => {
      this.currentNews.id = id;
      this.currentNews.title = response[0].title;
      this.currentNews.content = response[0].content;
      this.currentNews.time = response[0].time;
      this.currentNews.picture = response[0].picture;
      this.currentNews.category = response[0].category;
      this.currentNews.importance = response[0].importance;
    });
  }

  createNews(news: JSON) {
    return axios.post("/news", news).then(() => {
      // Update the news-array
      for (let e of this.news) {
        if (e.id == this.currentNews.id) {
          e.title = this.currentNews.title;
          e.content = this.currentNews.content;
          e.time = this.currentNews.time;
          e.picture = this.currentNews.picture;
          e.category = this.currentNews.category;
          e.importance = this.currentNews.importance;
          break;
        }
      }
      //this.currentNews = this.news.pop();
    });
  }

  updateNews(id: number, news: JSON) {
    return axios.put("/news/" + id, news).then(() => {
      // Update the news-array
      for (let e of this.news) {
        if (e.id == this.currentNews.id) {
          e.title = this.currentNews.title;
          e.content = this.currentNews.content;
          e.time = this.currentNews.time;
          e.picture = this.currentNews.picture;
          e.category = this.currentNews.category;
          e.importance = this.currentNews.importance;
          break;
        }
      }
    });
  }

  deleteNews(id: number) {
    return axios.delete("/news/" + id).then(() => {
      // Update the news-array
      for (let e of this.news) {
        if (e.id == this.currentNews.id) {
          e.title = this.currentNews.title;
          e.content = this.currentNews.content;
          e.time = this.currentNews.time;
          e.picture = this.currentNews.picture;
          e.category = this.currentNews.category;
          e.importance = this.currentNews.importance;
          break;
        }
      }
    });
  }
}
export let newsStore = sharedComponentData(new NewsStore());

// @flow

import * as React from "react";
import { Component } from "react-simplified";
import { HashRouter, Route, NavLink } from "react-router-dom";
import ReactDOM from "react-dom";
import { Alert, Card, NavBar, ListGroup } from "./widgets";
import { newsStore } from "./stores";

import createHashHistory from "history/createHashHistory";
const history = createHashHistory(); // Use history.push(...) to programmatically change path

class Menu extends Component {
  render() {
    return (
      <NavBar>
        <NavBar.Brand> Community News</NavBar.Brand>
        <NavBar.Link to="/Business">Business</NavBar.Link>
        <NavBar.Link to="/Sports">Sports</NavBar.Link>
        <NavBar.Link to="/Culture">Culture</NavBar.Link>
        <NavBar.Link to="/Science">Science</NavBar.Link>
        <NavBar.Link to="/Technology">Technology</NavBar.Link>
        <NavBar.Link to="/addnews">Add news</NavBar.Link>
      </NavBar>
    );
  }
}

class NewsList extends Component {
  //Only listed the news with the degree of importance 1 on the homepage
  render() {
    return (
      <Card title="">
        <Card title="">
          <h5>
            <b>Latest 5 News</b>
          </h5>
          {newsStore.latestnews.map(e => (
            <div class="card1">
              <NavLink
                activeStyle={{ color: "darkblue" }}
                exact
                to={"/news/" + e.id}
              >
                {e.title}
              </NavLink>{" "}
              {e.time}
            </div>
          ))}
        </Card>
        <Card title="">
          <ul>
            {newsStore.importantnews.map(e => (
              <div class="card2">
                <li key={e.id}>
                  <NavLink
                    activeStyle={{ color: "darkblue" }}
                    exact
                    to={"/news/" + e.id}
                  >
                    {e.title}
                  </NavLink>
                  <br />
                  <NavLink
                    activeStyle={{ color: "darkblue" }}
                    to={"/news/" + e.id + "/edit"}
                  >
                    edit
                  </NavLink>
                  <br />
                  <NavLink
                    activeStyle={{ color: "black" }}
                    to={"/news/" + e.id + "/delete"}
                  >
                    delete
                  </NavLink>
                  <br />
                  <img src={e.picture} height="200" width="300" />
                </li>
              </div>
            ))}
          </ul>
        </Card>
      </Card>
    );
  }
  mounted() {
    newsStore.getAllNews().catch((error: Error) => Alert.danger(error.message));
    newsStore
      .getLatestNews()
      .catch((error: Error) => Alert.danger(error.message));
    /*var max = 10; //length of one page
    var page; // pages in total
    pages = [];
    if (newsStore.importantnews.length % max == 0) {
      page = newsStore.importantnews.length / max;
    } else {
      page = Math.ceil(newsStore.importantnews.length / max);
    }
    for (i = 0; i < page; i++) {
      pages.push(i + 1);
    }*/
  }
}

class TypeList extends Component<{ match: { params: { type: string } } }> {
  render() {
    return (
      <Card title="">
        <ul>
          {newsStore.news.map(e => (
            <div class="card">
              <li key={e.id}>
                <NavLink
                  activeStyle={{ color: "darkblue" }}
                  exact
                  to={"/news/" + e.id}
                >
                  {e.title}
                </NavLink>
                <br />
                <NavLink
                  activeStyle={{ color: "black" }}
                  to={"/news/" + e.id + "/edit"}
                >
                  edit
                </NavLink>
                <br />
                <NavLink
                  activeStyle={{ color: "black" }}
                  to={"/news/" + e.id + "/delete"}
                >
                  delete
                </NavLink>
                <br />
                <img src={e.picture} height="200" width="300" />
              </li>
            </div>
          ))}
        </ul>
      </Card>
    );
  }

  mounted() {
    newsStore
      .getTypeNews(this.props.match.params.type)
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class NewsDetails extends Component<{ match: { params: { id: number } } }> {
  render() {
    return (
      <Card title="">
        <h5 class="card-title">{newsStore.currentNews.title}</h5>
        <h6 class="card-subtitle mb-2 text-muted">
          Time: {newsStore.currentNews.time}
          <br />
          Category: {newsStore.currentNews.category}
          <br />
          Degree of Importance: {newsStore.currentNews.importance}
        </h6>
        <br />
        <img src={newsStore.currentNews.picture} height="300" width="400" />
        <br />
        <br />
        <p class="card-text">
          <pre>{newsStore.currentNews.content}</pre>
        </p>
      </Card>
    );
  }

  mounted() {
    newsStore
      .getNews(this.props.match.params.id)
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class NewsRegister extends Component {
  title = "";
  content = "";
  picture = null;
  category = "Business";
  importance = 1;
  form = null;

  render() {
    return (
      <Card title="Register News">
        <form ref={e => (this.form = e)}>
          <div class="form-group">
            <label>Title</label>
            <input
              type="text"
              class="form-control"
              id="title"
              onChange={event => (this.title = event.target.value)}
              required
            />
          </div>
          <div class="form-group">
            <label for="newsFormContent">Content</label>
            <textarea
              class="form-control"
              id="content"
              rows="4"
              onChange={event => (this.content = event.target.value)}
            />
          </div>
          <div class="form-group">
            <label for="newsFormPicture">Picture</label>
            <input
              type="text"
              class="form-control"
              id="picture"
              onChange={event => (this.picture = event.target.value)}
              placeholder="Write URL here"
            />
          </div>
          <div class="form-group">
            <label for="newsFormCategory">Category</label>
            <select
              class="form-control"
              id="category"
              onChange={event => (this.category = event.target.value)}
              required
            >
              <option>Business</option>
              <option>Sports</option>
              <option>Culture</option>
              <option>Science</option>
              <option>Technology</option>
            </select>
          </div>
          <div class="form-group">
            <label for="newsFormImportance">Degree of Importance</label>
            <select
              class="form-control"
              id="importance"
              onChange={event => (this.importance = event.target.value)}
              required
            >
              <option>1</option>
              <option>2</option>
            </select>
          </div>
        </form>
        <button type="button" class="btn btn-success" onClick={this.create}>
          Submit
        </button>
        <button
          type="button"
          class="btn btn-light"
          onClick={() => history.push("/")}
        >
          Cancel
        </button>
      </Card>
    );
  }
  create() {
    var now = new Date();
    let now_format =
      now.getFullYear() +
      "/" +
      (now.getMonth() + 1) +
      "/" +
      now.getDate() +
      " " +
      now.getHours() +
      ":" +
      (now.getMinutes() < 10 ? "0" : "") +
      now.getMinutes();
    if (!this.form || !this.form.checkValidity()) return;
    let news_new = {
      title: this.title,
      content: this.content,
      time: now_format,
      picture: this.picture,
      category: this.category,
      importance: this.importance
    };
    newsStore
      .createNews(news_new)
      .catch((error: Error) => Alert.danger(error.message));
    history.push("/");
  }
}

class NewsEdit extends Component<{ match: { params: { id: number } } }> {
  form = null;
  render() {
    return (
      <Card title="Edit News">
        <form ref={e => (this.form = e)}>
          <div class="form-group">
            <label for="newsFormTitle">Title</label>
            <input
              type="text"
              class="form-control"
              id="title"
              value={newsStore.currentNews.title}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (newsStore.currentNews.title = event.target.value)
              }
            />
          </div>
          <div class="form-group">
            <label for="newsFormContent">Content</label>
            <textarea
              class="form-control"
              id="content"
              rows="4"
              value={newsStore.currentNews.content}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (newsStore.currentNews.content = event.target.value)
              }
            />
          </div>
          <div class="form-group">
            <label for="newsFormPicture">Picture[URL]</label>
            <input
              type="text"
              class="form-control"
              id="picture"
              value={newsStore.currentNews.picture}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (newsStore.currentNews.picture = event.target.value)
              }
            />
          </div>
          <div class="form-group">
            <label for="newsFormCategory">Category</label>
            <select
              class="form-control"
              id="category"
              value={newsStore.currentNews.category}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (newsStore.currentNews.category = event.target.value)
              }
              required
            >
              <option>Business</option>
              <option>Sports</option>
              <option>Culture</option>
              <option>Science</option>
              <option>Technology</option>
            </select>
          </div>
          <div class="form-group">
            <label for="newsFormImportance">Degree of Importance</label>
            <select
              class="form-control"
              id="importance"
              value={newsStore.currentNews.importance}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (newsStore.currentNews.importance = event.target.value)
              }
              required
            >
              <option>1</option>
              <option>2</option>
            </select>
          </div>
        </form>
        <button type="button" class="btn btn-success" onClick={this.save}>
          Save
        </button>
        <button
          type="button"
          class="btn btn-light"
          onClick={() => history.push("/")}
        >
          Cancel
        </button>
      </Card>
    );
  }

  mounted() {
    newsStore
      .getNews(this.props.match.params.id)
      .catch((error: Error) => Alert.danger(error.message));
  }

  save() {
    var now = new Date();
    let now_format =
      now.getFullYear() +
      "/" +
      (now.getMonth() + 1) +
      "/" +
      now.getDate() +
      " " +
      now.getHours() +
      ":" +
      (now.getMinutes() < 10 ? "0" : "") +
      now.getMinutes();
    let news_update = {
      title: newsStore.currentNews.title,
      content: newsStore.currentNews.content,
      time: now_format,
      picture: newsStore.currentNews.picture,
      category: newsStore.currentNews.category,
      importance: newsStore.currentNews.importance
    };
    newsStore
      .updateNews(newsStore.currentNews.id, news_update)
      .then(() => history.push("/news/" + newsStore.currentNews.id))
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class NewsDelete extends Component<{ match: { params: { id: number } } }> {
  render() {
    return null;
  }
  mounted() {
    if (confirm("Are you sure to delete?")) {
      newsStore
        .deleteNews(this.props.match.params.id)
        .then(() => history.push("/"))
        .catch((error: Error) => Alert.danger(error.message));
    } else {
      history.push("/");
    }
  }
}

const root = document.getElementById("root");
if (root) {
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <Menu />
        <Route exact path="/" component={NewsList} />
        <Route exact path="/addnews" component={NewsRegister} />
        <Route exact path="/news" component={NewsList} />
        <Route exact path="/:type" component={TypeList} />
        <Route exact path="/news/:id" component={NewsDetails} />
        <Route exact path="/news/:id/edit" component={NewsEdit} />
        <Route exact path="/news/:id/delete" component={NewsDelete} />
      </div>
    </HashRouter>,
    root
  );
}

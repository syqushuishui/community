// Make a copy of newsdao but change the db(news) to testdb(testnews) for testing the function
const Dao = require("../src/dao.js");

module.exports = class NewsDaoCopy extends Dao {
  getAll(callback) {
    super.query(
      "select id, title, content, time, picture, category, importance from testnews",
      [],
      callback
    );
  }

  getOne(id, callback) {
    super.query(
      "select title, content, time, picture, category, importance from testnews where id=?",
      [id],
      callback
    );
  }

  //insert
  createOne(json, callback) {
    var val = [
      json.title,
      json.content,
      json.time,
      json.picture,
      json.category,
      json.importance
    ];
    super.query(
      "insert into testnews (title, content, time, picture, category, importance) values (?,?,?,?,?,?)",
      val,
      callback
    );
  }

  // update
  updateOne(id, json, callback) {
    var val = [
      json.title,
      json.content,
      json.time,
      json.picture,
      json.category,
      json.importance,
      id
    ];
    super.query(
      "update testnews set title=?,content=?,time=?, picture=?, category=?, importance=? where id=?",
      val,
      callback
    );
  }

  // delete
  deleteOne(id, callback) {
    super.query("delete from testnews where id=?", [id], callback);
  }

  // get latest 5 news (title+time)
  getLatest(callback) {
    super.query(
      "select id, title, content, time, picture, category, importance from testnews order by time DESC LIMIT 5",
      [],
      callback
    );
  }
};

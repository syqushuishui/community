var mysql = require("mysql2");

const NewsDaoCopy = require("./newsdaocopy.js");
const runsqlfile = require("./runsqlfile.js");

/*GitLab CI Pool
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql",
  user: "root",
  password: "secret",
  database: "supertestdb",
  debug: false,
  multipleStatements: true
});*/

//Test with my db
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql.stud.iie.ntnu.no",
  user: "shanshaq",
  password: "eZw30TPV",
  database: "shanshaq",
  debug: false,
  multipleStatements: true
});

let newsDao = new NewsDaoCopy(pool);

beforeAll(done => {
  runsqlfile("tests/create_table.sql", pool, () => {
    runsqlfile("tests/create_testdata.sql", pool, done);
  });
});

afterAll(() => {
  pool.end();
});

test("get one record from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].title).toBe("TestNews1");
    done();
  }

  newsDao.getOne(1, callback);
});

test("get unknown record from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  newsDao.getOne(0, callback);
});

test("add news to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  newsDao.createOne(
    {
      title: "Marvel legend Stan Lee dies at 95",
      content:
        "Marvel legend Stan Lee, who revolutionized pop culture as the co-creator of iconic superheroes like Spider-Man and The Hulk who now dominate the world's movie screens, has died. He was 95 years old. Lee, the face of comic book culture in the United States, died early Monday in Los Angeles, according to US entertainment outlets including....",
      time: "2018-11-13 14:30:00",
      picture: null,
      category: 3,
      importance: 1
    },
    callback
  );
});

test("get all news from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(2);
    done();
  }

  newsDao.getAll(callback);
});

test("update news to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" +
        status +
        ", data.length=" +
        JSON.stringify(data)
    );
    expect(data.changedRows).toBe(1);
    newsDao.getOne(2, callback2);
  }
  function callback2(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].title).toBe("Marvel legend Stan Lee dies at 95");
    done();
  }

  newsDao.updateOne(
    2,
    {
      title: "Marvel legend Stan Lee dies at 95",
      content:
        "Marvel legend Stan Lee, who revolutionized pop culture as the co-creator of iconic superheroes like Spider-Man and The Hulk who now dominate the world's movie screens, has died. He was 95 years old. Lee, the face of comic book culture in the United States, died early Monday in Los Angeles, according to US entertainment outlets including....",
      time: "2018/11/13 14:30",
      picture: null,
      category: "Culture",
      importance: 1
    },
    callback
  );
});

test("delete a record from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" +
        status +
        ", data.length=" +
        JSON.stringify(data)
    );
    expect(data.affectedRows).toEqual(1);
    newsDao.getAll(callback2);
  }

  function callback2(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(2);
    done();
  }

  newsDao.deleteOne(3, callback);
});

DROP TABLE IF EXISTS testnews;

CREATE TABLE testnews  (
  id int(11) NOT NULL AUTO_INCREMENT,
  title varchar(256) NOT NULL,
  content longtext NOT NULL,
  time varchar(36),
  picture longtext DEFAULT NULL,
  category varchar(36),
  importance int(11),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

// @flow
var path = require("path");
var express = require("express");
var mysql = require("mysql2");
var bodyParser = require("body-parser");
var apiRoutes = express.Router();
let app = express();
app.use(bodyParser.json()); // for å tolke JSON
const NewsDao = require("./newsdao.js");

/*type Request = express$Request;
type Response = express$Response;*/

const public_path = path.join(__dirname, "/../../client/public");

app.use(express.static(public_path));
app.use(express.json()); // For parsing application/json

var pool = mysql.createPool({
  connectionLimit: 2,
  host: "mysql.stud.iie.ntnu.no",
  user: "shanshaq",
  password: "eZw30TPV",
  database: "shanshaq",
  debug: false
});

let newsDao = new NewsDao(pool);

// Get all records
app.get("/news", (req, res) => {
  newsDao.getAll((status, data) => {
    res.status(status);
    res.json(data);
  });
});

// Get one record
app.get("/news/:id", (req, res) => {
  newsDao.getOne(req.params.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

// Create a new record
app.post("/news", (req, res) => {
  console.log(req.body);
  newsDao.createOne(req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

// Update a record
app.put("/news/:id", (req, res) => {
  newsDao.updateOne(req.params.id, req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

// Delete a record
app.delete("/news/:id", (req, res) => {
  newsDao.deleteOne(req.params.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

// Get the time and the title of the latest 5 news for "live" newsfeed
app.get("/latestnews", (req, res) => {
  newsDao.getLatest((status, data) => {
    res.status(status);
    res.json(data);
  });
});

var server = app.listen(8080);

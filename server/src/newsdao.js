const Dao = require("./dao.js");

module.exports = class NewsDao extends Dao {
  getAll(callback) {
    super.query(
      "select id, title, content, time, picture, category, importance from news",
      [],
      callback
    );
  }

  getOne(id, callback) {
    super.query(
      "select title, content, time, picture, category, importance from news where id=?",
      [id],
      callback
    );
  }

  //insert
  createOne(json, callback) {
    var val = [
      json.title,
      json.content,
      json.time,
      json.picture,
      json.category,
      json.importance
    ];
    super.query(
      "insert into news (title, content, time, picture, category, importance) values (?,?,?,?,?,?)",
      val,
      callback
    );
  }

  // update
  updateOne(id, json, callback) {
    var val = [
      json.title,
      json.content,
      json.time,
      json.picture,
      json.category,
      json.importance,
      id
    ];
    super.query(
      "update news set title=?,content=?,time=?, picture=?, category=?, importance=? where id=?",
      val,
      callback
    );
  }

  // delete
  deleteOne(id, callback) {
    super.query("delete from news where id=?", [id], callback);
  }

  // get latest 5 news (title+time)
  getLatest(callback) {
    super.query(
      "select id, title, content, time, picture, category, importance from news order by time DESC LIMIT 5",
      [],
      callback
    );
  }
};

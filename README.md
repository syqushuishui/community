# Community News(Øving3+12)

A very simple web-app based on the examples in the class TDAT2003 Systemutvikling 2 med web-applikasjoner

## Client: run tests and start

From the top-level repository folder:

```sh
cd client
npm install
npm test
npm start
```

## Server: run tests and start

From the top-level repository folder:

```sh
cd server
npm install
npm test
npm start
```

## Open application

http://localhost:8080
